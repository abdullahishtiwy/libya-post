<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

function translations($json)
{
    php_to_json();

    if (!file_exists($json)) {
        return [];
    }

    return json_decode(file_get_contents($json), true);
}

function php_to_json()
{
    foreach (['ar', 'en'] as $locale) {
        $path = base_path("lang/$locale");

        $trans = collect(File::allFiles($path))->flatMap(function ($file) use ($locale) {
            $key = ($translation = $file->getBasename('.php'));
    
            return [$key => trans($translation, [], $locale)];
        });


        $translated_file = Arr::dot($trans);

        $fileContents = json_encode($translated_file);
        File::put(base_path('lang/' . $locale . '.json'), $fileContents);
    }
}
