<?php

return [
    'dir' => 'rtl',
    'locale' => 'ar-LY',
    'login' => 'تسجيل دخول',
    'register' => 'تسجيل حساب',
    'dashboard' => 'لوحة التحكم'
];