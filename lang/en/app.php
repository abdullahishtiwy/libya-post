<?php

return [
    'dir' => 'ltr',
    'locale' => 'en-US',
    'login' => 'Login',
    'register' => 'Register',
    'dashboard' => 'Dashboard'
];